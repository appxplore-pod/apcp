# apcp

A library created by Appxplore for accessing the Cross Promotion Tools built under it.

In order to submit a new version of pod, please do following:
* Add a pod repo to your terminal first, for eg. 
"pod repo add appxplore-pod https://gitlab.com/appxplore-pod/podspec.git"

* build the frameworks first.(Another repository story)
* on the Pod-Frameworks, check the pod validation first:
"pod spec lint --allow-warnings"

* after it get passed, submit the pod:
"pod repo push appxplore-pod ./apcp.podspec --allow-warnings"
