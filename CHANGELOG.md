1.7.0
========
1. added support for Arabic Language for Privacy Policy and ATT.
2. Built using XCode 13.2.1

1.6.2
========
1. Added Session End Event.
2. Added variable to determine session end which read from initialization.
3. When go to background, should send the session end event immediately, but once back from background, will determine if pass a time(right now is 3 minutes), then will recreate another new session.

1.6.1
========
1. added Debug Log for Push Notification Manager, so we know what happens for push notifications.
2. add a prevention before initialization to make sure the url should be there before initialization.

1.6.0
========
1. Added update Push Token in the APCPromo.h
2. We added an APCPOperationQueue to have a queue in the frameworks so we can use it to send more and more events/web request in the future.
3. Added a new Function name setStagingServer in APCPromo.h, not really for developer, only for us internally only.
4. UpdatePushToken function has to be a valid and non-null string.

1.5.5
========
1. remove the must check ATT before initialize for iOS 14
2. cache the UUID once it has been used for initialize.
