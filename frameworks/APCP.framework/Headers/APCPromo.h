//
//  CPromo.h
//  CrossPromoExample
//
//  Created by Appxplore 14 on 1/12/17.
//  Copyright © 2017 Appxplore Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <StoreKit/StoreKit.h>


typedef NS_ENUM(NSInteger, APCPATTStatus)
{
    kATTNotSupported = -2,
    kATTNotKnown = 0,
    kATTSuccess = 1,
    kATTDenied = -1
};


typedef NS_ENUM(NSInteger, APCPMoreGameCachedError)
{
    kMGSuccess,
    kMGNotInit,
    kMGCacheRequestError,
    kMGCacheError
};

typedef NS_ENUM(NSInteger, kAPCPViewAnimation)
{
    kViewAnimationSlideFromBottom = 0,
    kViewAnimationSlideFromTop = 1,
    kViewAnimationSlideFromLeft = 2,
    kViewAnimationSlideFromRight = 3,
};

NS_ASSUME_NONNULL_BEGIN
@protocol APCPDelegate <NSObject>

- (void)APCPInitDone;
- (void)APCPInitError:(NSString *) errorMsg;

- (void)APCPMoreGamesClosed;
- (void)APCPMoreGamesCachedReady;
- (void)APCPMoreGamesCachedError:(NSString *) errorMsg;

- (void)APCPInterstitialClosed:(NSString *)locationKey;
- (void)APCPInterstitialCachedReady:(NSString *)locationKey;
- (void)APCPInterstitialCachedError:(NSString *)locationKey ErrorMsg:(NSString *) errorMsg;
- (void)APCPConsentDialogResult:(BOOL) consentGiven;

- (void)APCPATTPermissionResult:(APCPATTStatus) attStatus;
@end


@interface APCPromo : NSObject

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)initialize NS_UNAVAILABLE;

+ (void)initialize:(NSString *)gameId delegate:(nullable id<APCPDelegate>)delegate testMode:(BOOL) isTestMode;
+ (id<APCPDelegate>)getDelegate;
+ (void)connect;
+ (void)setCustomId:(NSString *)customId;
+ (void)setCustomLang:(NSString *)customLang;
+ (void)setStagingServer;

+ (BOOL)isInitialized;

+ (void)setAutoCacheMoreGameOnStartOnly:(BOOL) isCache;
+ (void)setAutoCacheMoreGame:(BOOL) autocache;
+ (void)setMoreGameAnimationType:(kAPCPViewAnimation) animType;
+ (BOOL)isMoreGameReady;
+ (void)cacheMoreGames;
+ (BOOL)showMoreGames:(UIViewController *)rootView;

+ (void)setAutoCacheInterstitial:(BOOL) autocache;
+ (void)setInterstitialAnimationType:(kAPCPViewAnimation) animType;
+ (void)setInterstitialLocationList:(NSArray *)listOfInterstitialLocations;
+ (BOOL)isInterstitialReady:(NSString *)locationKey;
+ (void)cacheInterstitial:(NSString *)locationKey;
+ (void)cacheAllInterstitials;
+ (BOOL)showInterstitial:(NSString *)locationKey RootView:(UIViewController *)rootView;

+ (void) setDeepLinkData:(NSURL *) url;
+ (NSString *) getDeepLinkData;
+ (void) removeDeepLinkData;

+ (void) setConsent:(BOOL) consentGiven;
+ (BOOL) isConsentGiven;
+ (void) showConsentDialog: (NSString *) privacyPolicyUrl RootView:(UIViewController *)rootView;

+ (APCPATTStatus) ATTPermissionStatus;
+ (void) showATTPermission: (BOOL) showDialog RootView:(UIViewController *)rootView;

+ (void) updatePushToken: (NSString *) pushToken;


+ (void)applicationDidBecomeActive:(UIApplication *)application;
+ (void)applicationWillResignActive:(UIApplication *)application;
//+ (void)applicationWillTerminate:(UIApplication *)application;
@end

NS_ASSUME_NONNULL_END
